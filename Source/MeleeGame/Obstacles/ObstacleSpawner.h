// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Containers/List.h"


#include "ObstacleSpawner.generated.h"

UCLASS()
class MELEEGAME_API AObstacleSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AObstacleSpawner();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Obstacle")
	TArray<AActor*> spawnedObjs;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Obstacle")
	float spawnXPosition;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Obstacle")
	float minBoxSpawnY;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Obstacle")
	float maxBoxSpawnY;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Obstacle")
	float boxSpawnZ;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Obstacle")
	float floorSpawnY;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Obstacle")
	float floorSpawnZ;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Obstacle")
	float timeBetweenBoxSpawns = 4.0f;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Obstacle")
	void SpawnBoxes();
	
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Obstacle")
	void SpawnFloors();

	UFUNCTION(BlueprintCallable, Category = "Obstacle")
	void SetTimeBetweenBoxSpawns(float newTime);
};
